
# GIT

# Pushing to a remote branch with a different branch name

```bash
git push origin local-name:remote-name
```
# Undo previous commit

```bash
git reset --soft HEAD~1
```

# PACMAN

# Finding out info about a package

```bash
pacman -Q --info package
```
